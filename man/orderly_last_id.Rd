% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/query.R
\name{orderly_last_id}
\alias{orderly_last_id}
\title{Get id of last run report}
\usage{
orderly_last_id(config = NULL, locate = TRUE, draft = TRUE)
}
\arguments{
\item{config}{An orderly configuration, or the path to one (or
\code{NULL} to locate one if \code{locate} is \code{TRUE}).}

\item{locate}{Logical, indicating if the configuration should be
searched for.  If \code{TRUE} and \code{config} is not given,
then orderly looks in the working directory and up through its
parents until it finds an \code{orderly_config.yml} file.}

\item{draft}{Find draft reports?}
}
\description{
Find the last id that was run
}
